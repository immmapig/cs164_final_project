var Sprites = function(GP, type) {
    
    var S = function() {};
    S.gp = GP;
    S.type = type;
    S.hitMap = {};
    S.uid = 0;
    S.can_move = false;
    S.direction = 'right';
    S.behavior = null;
    S.count = 0;
    S.ladder = false;
    S.priority = -1;
    S.grounded = false;
    S.xvelocity = 0.0;
    S.yvelocity = 0.0;
    S.visible = true;

    S.hit = function(collide) {
        if(this.hitMap.hasOwnProperty(collide.type)) {
            return this.hitMap[collide.type](collide, this);
        }
        return false;
    };

    S.addHit = function(collide_type, func) {
        this.hitMap[collide_type] = func;
        return this;
    }

    S.respawn = function(time, x, y) {
        setTimeout(function() {
            setInterval(function() {
                gp.toAdd.push(S.clone().setCoordinates(x*3+1, y*3+1))
            }, time);
        }(), time);
        return this;
    }

    S.destroy = function() {
        this.gp.toDestroy.push(this);
        S.count -= 1;
        return this;
    } 

    S.setCoordinates = function(x,y) {
        this.x = x;
        this.y = y;
        return this;
    }

    S.step = function(){
        var temp = this.behavior();
        return temp;
    }

    S.setBehavior = function(behavior_func) {
        this.behavior = behavior_func;
        return this;
    }

    S.setDirection = function(dir) {
        this.direction = dir;
        return this;
    }

    S.moves = function(type) {
        this.can_move = true;
        return this;
    };

    S.moveGravity = function(dir) {
        var x = this.x;
        var y = this.y;
        if (!this.grounded) {
            this.yvelocity += 1;
            y += Math.min(1, this.yvelocity)
        }

        if (dir) {
            dir = dir.split(" ");

            if (dir.indexOf('left') >= 0) {
                x -= 1;
            } 
            if (dir.indexOf('right') >= 0) {
                x += 1;
            }
            if (dir.indexOf('up') >= 0 && this.grounded) {
                this.yvelocity = -5;
                y += this.yvelocity;
                this.grounded = false;
            }
        }
        return [x, y];                    
    }

    S.isGrounded = function() {
        this.grounded = true;
        return this;
    }

    S.movePosition = function(dir) {
        var x = parseInt(this.x);
        var y = parseInt(this.y);
        switch(dir) {
            case 'left':
                return [x - 1, y];
            case 'right':
                return [x + 1, y];
            case 'up':
                return [x, y - 1];
            case 'down':
                return [x, y + 1];
        }
        return [x, y];
    }

    S.moveContinuous = function() {
        return this.movePosition(this.direction);
    }

    S.moveRandomly = function(directions, probability) {
        var changeDirection = Math.random() <= probability;
        if (!this.hasMoved) {
            changeDirection = true;
        }
        if (changeDirection) {
            removeFromArray(directions, this.direction);
            var chance = Math.random();
            this.direction = directions[Math.floor(chance*directions.length)]
        }
        return this.moveContinuous();
    }

    S.getInput = function() {
        return this.gp.ioQueue.pop();
    }

    S.controls = function() {
        var input = this.getInput();
        if(input != undefined) {
            this.setDirection(input);
        }
        return input;
    }

    S.setControls = function(control_func) {
        this.controls = control_func;
        return this;
    }


    S.clone = function(){
        S.count += 1;
        var s1 = function() {};
        for(key in S){
            s1[key] = S[key];
        }
        s1.id = S.genUID();
        return s1;
    }


    S.genUID = function(){
        S.uid += 1;
        return S.type + S.uid;
    }

    S.setPriority = function(priority){
        S.priority = priority;
        return this;
    }

    S.setGravity = function() {
        S.gravity = true;
        return this;
    }

    S.setVisible = function(){
        this.visible = true;
        gp.changed_sprites.push(this);
        return this;
    }

    S.setInvisible = function(){
        this.visible = false;
        return this;
    }

    S.gp.sprite_types[type] = S;


    return S;
};

