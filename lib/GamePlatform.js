
var GamePlatform = function(opts) {
    
    var GP = function() {};

    GP.tilesize = 11;
    GP.type_map = {};
    GP.gameMap = [];
    GP.imageMap = {};
    GP.sprite_types = {};
    GP.ioQueue = [];
    GP.nonmoving_sprites = [];
    GP.moving_sprites = [];
    GP.toDestroy = [];
    GP.toAdd = [];
    GP.changed_sprites = [];

    GP.setMap = function(filename){
        GP.mapfile = filename;
    }

    GP.initializeEnv = function() {
        //TODO

        $.getJSON(GP.mapfile, function(json) {
            for(entry in json) {
                for(c_index in json[entry].coord) {
                    coordinate = json[entry].coord[c_index];
                    coordinate = coordinate.substring(1, coordinate.length-1);
                    if(!GP.type_map.hasOwnProperty(json[entry].type)) {
                        GP.type_map[json[entry].type] = [];
                    }

                    var xy = coordinate.split(",");
                    var xrange = xy[0].split("-");
                    var yrange = xy[1].split("-");

                    var xlow = parseInt(xrange[0])
                    var xhigh = xrange.length > 1 ? parseInt(xrange[1]) : parseInt(xrange[0])

                    var ylow = parseInt(yrange[0])
                    var yhigh = yrange.length > 1 ? parseInt(yrange[1]) : parseInt(yrange[0])

                    for(i = xlow; i <= xhigh; i++){
                        for(j = ylow; j <= yhigh; j++){
                            GP.type_map[json[entry].type].push([i, j]);
                        }
                    }
                }
            }
            GP.startGameLoop();
        });
        return this;
    };

    GP.setSize = function(width, height) {
        GP.width = width * 3;
        GP.height = height * 3;
        return this;
    };

    GP.createEmptyGameMap = function() {
        var empty_map = [];
        for(i = 0; i < GP.height; i++) {
            for(j = 0; j < GP.width; j++) {
                empty_map[(i*GP.width)+j] = [];
            }
        }
        return empty_map;
    };


    GP.toString = function() {
        var result = "";
        for(i = 0; i < GP.height; i++) {
            for(j = 0; j < GP.width; j++) {
                var sprite_instance = GP.gameMap[(i*GP.width)+j];
                if(sprite_instance != "0") {
                    sprite_instance = sprite_instance.id;
                }
                result += sprite_instance + "\t";
            }
            result += "\n";
        }
        return result;
    };

    GP.coordinateTranslate = function(x, y) {
        return x + (y*GP.width);
    };

    GP.updateMapCoordinate = function(xy, sprite_instance) {
        var x = Math.min(Math.max(0, xy[0]), GP.width);
        var y = Math.min(Math.max(0, xy[1]), GP.height);
        GP.removeSprite(sprite_instance);
        GP.addSprite(x, y, sprite_instance);
        sprite_instance.setCoordinates(x, y);
    };

    GP.getSpriteList = function(x, y) {
        return GP.gameMap[GP.coordinateTranslate(x, y)];
    }

    GP.addSprite = function(x, y, sprite_instance) {
        var sprite_list = GP.gameMap[GP.coordinateTranslate(x, y)];
        sprite_list.push(sprite_instance);
    }

    GP.removeSprite = function(sprite_instance) {
        var sprite_list = GP.gameMap[GP.coordinateTranslate(sprite_instance.x, sprite_instance.y)];
        removeFromArray(sprite_list, sprite_instance);
    }
    
    GP.setBackground = function(color) { 
        GP.background = color;
        return this;
    };

    GP.importImage = function(type, filename) {
        GP.imageMap[type] = filename;
        return this;
    };

    GP.start = function() {
        GP.gameover = false;

        //background
        GP.svg = d3.select("body")
                  .append("svg")
                  .attr("width", GP.width*GP.tilesize)
                  .attr("height", GP.height*GP.tilesize)
                  .append("g");

        GP.svg.append("rect")
                  .attr("width", GP.width*GP.tilesize)
                  .attr("height", GP.height*GP.tilesize)
                  .attr("fill",GP.background);

        GP.initializeEnv();
    };

    GP.startGameLoop = function() {
        GP.gameMap = GP.createEmptyGameMap();
        for(type in GP.type_map) {
            for(sprite in GP.type_map[type]) {
                var sprite_instance = GP.sprite_types[type].clone();
                if(sprite_instance.can_move) {
                    GP.moving_sprites.push(sprite_instance);
                } else {
                    GP.nonmoving_sprites.push(sprite_instance);
                }
                var x = parseInt(GP.type_map[type][sprite][0]*3)+1;
                var y = parseInt(GP.type_map[type][sprite][1]*3)+1;
                sprite_instance.setCoordinates(x, y);
                GP.updateMapCoordinate([x,y], sprite_instance);
            }
        }

        GP.svg.selectAll(".sprite")
                .data([].concat.apply([], GP.gameMap).sort(function(a,b){return a.priority - b.priority}), function(d) {return d.id;})
                .enter()
                .append('svg:image')
                .attr('xlink:href', function(d) {return GP.imageMap[d.type]})
                .attr("width", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}})
                .attr("height", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}})
                .attr("class", "sprite")
                .attr("x", function(d){return d.x * GP.tilesize})
                .attr("y", function(d){return d.y * GP.tilesize}); 
        
        GP.runGameLoop();
    }

    GP.detectCollisions = function(transitions) {
        var destinations = {};
        for (loc in transitions) {
            loc = loc.split(',');
            var x = parseInt(loc[0]);
            var y = parseInt(loc[1]);
            var spriteList = transitions[loc];
            for (sprite_index in spriteList) {
                var sprite_instance = spriteList[sprite_index];
                var collided = [sprite_instance];
                var alone = true;
                var to_move = true;
                for (i = Math.max(0, x - 2); i <= Math.min(GP.width - 1, x + 2); i++) {
                    for (j = Math.max(0, y - 2); j <= Math.min(GP.height - 1, y + 2); j++) {
                        var occupiers = GP.gameMap[GP.coordinateTranslate(i, j)];
                        for (k in occupiers) {
                            var occupier = occupiers[k];
                            if (collided.indexOf(occupier) == -1) {
                                alone = false;
                                collided.push(occupier);
                                to_move = to_move && sprite_instance.hit(occupier, loc);
                            }
                        }
                    }
                }
                if (alone || to_move) {
                    destinations[loc] = sprite_instance;
                    sprite_instance.hasMoved = true;
                    if (alone) {
                        sprite_instance.grounded = false;
                    }
                } else {
                    sprite_instance.hasMoved = false;
                }
            }
        }
        return destinations;
    }

    GP.runGameLoop = function() {
        GP.running = setInterval(function(){
            var transitions = {};
            for(sprite_instance_index in GP.moving_sprites){
                var sprite_instance = GP.moving_sprites[sprite_instance_index]
                var next_step = sprite_instance.step();
                if(!transitions.hasOwnProperty(next_step)) {
                   transitions[next_step] = [];  
                }
                transitions[next_step].push(sprite_instance);
            }
            var destinations = GP.detectCollisions(transitions);

            // Destroys sprites after start
            for (i in GP.toDestroy) {
                to_destroy_sprite = GP.toDestroy[i];
                if (to_destroy_sprite.can_move) {
                    removeFromArray(GP.moving_sprites, to_destroy_sprite);
                } else {
                    removeFromArray(GP.nonmoving_sprites, to_destroy_sprite);
                }
                GP.removeSprite(to_destroy_sprite);
            }
            GP.svg.selectAll(".sprite")
                    .data(GP.toDestroy, function(d) {return d.id})
                    .remove();
            GP.toDestroy = [];

            // Adds sprites after start
            for (i in GP.toAdd) {
                to_add_sprite = GP.toAdd[i];
                if (to_add_sprite.can_move) {
                    GP.moving_sprites.push(to_add_sprite);
                } else {
                    GP.nonmoving_sprites.push(to_add_sprite);
                }
                GP.updateMapCoordinate([to_add_sprite.x, to_add_sprite.y], to_add_sprite)
            }
            GP.svg.selectAll(".sprite")
                .data(GP.toAdd, function(d) {return d.id;})
                .enter()
                .append('svg:image')
                .attr('xlink:href', function(d) {return GP.imageMap[d.type]})
                .attr("width", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}})
                .attr("height", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}})
                .attr("class", "sprite")
                .attr("x", function(d){return d.x * GP.tilesize})
                .attr("y", function(d){return d.y * GP.tilesize}); 
            GP.toAdd = [];


            for (loc in destinations) {
                loc = loc.split(',');
                var x = parseInt(loc[0]);
                var y = parseInt(loc[1]);
                var sprite = destinations[loc]
                GP.updateMapCoordinate([x, y], sprite);
                GP.changed_sprites.push(sprite);
            }

            GP.svg.selectAll(".sprite")
                    .data(GP.changed_sprites, function(d) {return d.id})
                    .transition()
                    .duration(42)
                    .attr("x", function(d){return d.x * GP.tilesize})
                    .attr("y", function(d){return d.y * GP.tilesize})
                    .attr("width", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}})
                    .attr("height", function(d){ if(d.visible){return GP.tilesize * 3} else {return 0}});
            GP.changed_sprites = []
            //edge detection
        }, 42);
    }
    
    GP.end = function() {
        clearInterval(GP.running);
        GP.svg.append('svg:image')
                .attr('xlink:href', 'gameover.png')
                .attr("width", GP.width*GP.tilesize/2)
                .attr("height", GP.height*GP.tilesize/2)
                .attr("class", "sprite")
                .attr("x", GP.width*GP.tilesize/4)
                .attr("y", GP.width*GP.tilesize/4); 
    };
    
    GP.addDefaultControls = function(){
        KeyboardJS.on('left', function() {
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('left');
            }
        });

        KeyboardJS.on('right', function() {
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('right');
            }
        });

        KeyboardJS.on('up', function() {
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('up');
            }
        });

        KeyboardJS.on('down', function() {
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('down');
            }
        });
    };

    GP.addMDControls = function(){
        GP.addDefaultControls();
        KeyboardJS.on('up + left', function(){
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('up left');
            }
        });
        KeyboardJS.on('up + right', function(){
            if(GP.ioQueue.length < 3){
                GP.ioQueue.push('up right');
            }
        });

    }

    return GP;
};

var removeFromArray = function(array, item) {
    if (array.indexOf(item) != -1) {
        array.splice(array.indexOf(item), 1);
    }
}